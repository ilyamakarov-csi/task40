import com.google.common.base.Function;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * Created by IlyaMakarov on 12/15/2016.
 */
public class RmsysTest {
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        driver = new FirefoxDriver();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS); //implicit. Not needed due to explicit waiters.
    }

    @AfterClass
    public void tearDown() {
        driver.quit();
    }


    @DataProvider(name="loginData")
    public static Object[][] createData() {
        return new Object[][] {
                new Object[] { "IlyaMakarov", "123" },
                new Object[] { "ArthurAminov", "123" },
                new Object[] { "EugenBorisik", "123" },
                new Object[] { "ValeryH", "123" }
        };
    }


    @Test(dataProvider = "loginData")
    public void testLogin(String userNameInput, String passwordInput) throws InterruptedException {
        driver.get("https://192.168.100.26/");
        driver.findElement(By.id("Username")).sendKeys(userNameInput);
        driver.findElement(By.id("Password")).sendKeys(passwordInput);
        driver.findElement(By.id("SubmitButton")).click();

        //Thread.sleep(5000); //explicit

        WebDriverWait waiter = new WebDriverWait(driver, 10);
        WebElement signOut = waiter.until(ExpectedConditions.visibilityOfElementLocated(By.linkText("Sign Out")));
        Assert.assertNotNull(signOut);
    }

    @Test
    public void testSearchOfficeTextBox() throws InterruptedException  {
        driver.get("https://192.168.100.26/");
        driver.findElement(By.id("Username")).sendKeys("IlyaMakarov");
        driver.findElement(By.id("Password")).sendKeys("IlyaMakarov");

        driver.findElement(By.id("SubmitButton")).click();
        driver.findElement(By.id("officeMenu")).click();

        Wait<WebDriver> wait = new FluentWait<WebDriver>(driver)
                .withTimeout(15, SECONDS)
                .pollingEvery(3, SECONDS) // requested 2.7 seconds but function gets only long type
                .ignoring(NoSuchElementException.class);

        WebElement searchOfficeTextBox = wait.until(new Function<WebDriver, WebElement>() {
            public WebElement apply(WebDriver driver) {
                return driver.findElement(By.id("input-search"));
            }
        });

        Assert.assertTrue(searchOfficeTextBox.isDisplayed());
    }
}

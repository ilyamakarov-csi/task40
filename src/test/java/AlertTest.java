import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

/**
 * Created by IlyaMakarov on 12/19/2016.
 */
public class AlertTest {

    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        driver = new FirefoxDriver();
        driver.get("https://the-internet.herokuapp.com/javascript_alerts");

    }
    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void testSimpleAlert() {
        WebElement button = driver.findElement(By.cssSelector(".example>ul>li>button[onclick='jsAlert()']"));
        button.click();
        try {
            Alert alert = driver.switchTo().alert();
            String textOnAlert = alert.getText();
            alert.accept();
            assertEquals("I am a JS Alert",textOnAlert);
        } catch (NoAlertPresentException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testConfirmAlert() {
        WebElement button = driver.findElement(By.cssSelector(".example>ul>li>button[onclick='jsConfirm()']"));
        button.click();
        try {
            Alert alert = driver.switchTo().alert();
            String message = alert.getText();
            alert.accept();
            assertEquals("I am a JS Confirm", message);
        } catch (NoAlertPresentException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testPrompt() {
        WebElement button = driver.findElement(By.cssSelector(".example>ul>li>button[onclick='jsPrompt()']"));
        button.click();

        try {
            Alert alert = driver.switchTo().alert();
            String message = alert.getText();
            alert.accept();

            assertEquals("I am a JS prompt", message);
        } catch (NoAlertPresentException e) {
            e.printStackTrace();
        }
    }
}

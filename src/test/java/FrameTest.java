import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by IlyaMakarov on 12/19/2016.
 */
public class FrameTest {
    private WebDriver driver;

    @BeforeClass
    public void setUp() {
        driver = new FirefoxDriver();
    }
    @AfterClass
    public void tearDown() {
        driver.quit();
    }

    @Test
    public void InputFrame() {
        driver.get("https://the-internet.herokuapp.com/iframe");
        driver.switchTo().frame("mce_0_ifr");
        WebElement input = driver.findElement(By.cssSelector("#tinymce > p"));
        input.clear();

        Actions action = new Actions(driver);
        action.sendKeys(input, "Hello").build().perform();
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("var strongText = document.createElement('strong')\n" +
                "strongText.innerHTML = 'World!';" +
                "arguments[0].appendChild(strongText);",input);

        Assert.assertTrue(input.getText().equals("Hello World!"));
    }
}
